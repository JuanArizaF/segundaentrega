let bookmark = [
    {
        nombre: 'prueba',
        url: 'hola.com',
        comentario: 'esto es una prueba',
        label: "1",
        login: 'juan',
        password: '1234'
    },
    {
        nombre: 'prueba2',
        url: 'www.hola.com',
        comentario: 'esto es una prueba x2',
        label: "0",
        login: 'juan',
        password: '1234'
    },
    {
        nombre: 'prueba3',
        url: 'www.hola4.com',
        comentario: 'esto es una prueba x3',
        label: "0",
        login: 'juan',
        password: '1234'
    },
]
let labels = [
    {
        label: "NINGUNO"
    },
    {
        label: "hola"
    },
    {
        label: "adios"
    },
    {
        label: "buen dia"
    },
]



document.addEventListener("DOMContentLoaded", () => {
    // Boton buscar
    btn_inicar = document.getElementById("btn_ver_todos_bookmarks");
    btn_inicar.addEventListener("click", inciar);

    function inciar(evento) {
        evento.preventDefault();
        console.log(bookmark.length);

        for (let i = 0; i < bookmark.length; i++) {
            document.getElementById("tb_todos").insertRow(-1).innerHTML = `<td><div id="content${i}"></div></td><td><div id="content1${i}"></div></td><td><div id="content2${i}"></div></td>`;
            document.getElementById(`content${i}`).innerHTML = bookmark[i]['nombre'];
            document.getElementById(`content1${i}`).innerHTML = bookmark[i]['url'];
            document.getElementById(`content2${i}`).innerHTML = labels[bookmark[i]['label']]['label'];
        }

    }

})
